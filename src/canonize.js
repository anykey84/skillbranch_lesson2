
export  default function canonize(url) {
  const re = new RegExp('@?(http(s?)?:)?(\/\/)?(www.)?((telegram|vk|vkontakte|twitter|github)[^\/]*\/)?([a-z0-9.]*)', 'i');
  const username = url.match(re);
  console.log(username);
  return '@'+username[username.length-1];
}
