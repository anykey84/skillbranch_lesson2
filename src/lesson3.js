import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';
var cors = require('cors');


import canonize from './canonize';

const __DEV__ = true;

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

///////////////// канонизация url /////////////////////////////

app.get('/canonize', (req,res) => {
  console.log(req.query.username);
  res.send(canonize(req.query.username))
});

/////////////////// получение покемонов ///////////////////////////

const baseUrl = 'http://pokeapi.co/api/v2';
const pokemonFields = ['id', 'name', 'weight'];

async function getPokemons(url) {
  const response = await fetch(url);
  const page = await response.json();
  const pokemons = page.results;

  if (!__DEV__){
    if(page.next){
      const pokemons2 = await getPokemons(page.next);
      return [
        ...pokemons, ...pokemons2
      ]
    }
  }

  return pokemons;
}

async function getPokemon(url) {
  const response = await fetch(url);
  const pokemon = await response.json();
  return pokemon;
}



app.get('/pokemon', async (req,res) => {
  try {
    const pokemonsUrl = `${baseUrl}/pokemon`;
    const pokemonsInfo = await getPokemons(pokemonsUrl);
    const pokemonsPromises = pokemonsInfo.map(info => {
      //console.log(getPokemon(info.url));
      return getPokemon(info.url);
    });

    const pokemonsFull = await Promise.all(pokemonsPromises);
    //console.log(pokemonsFull);
    const pokemons = pokemonsFull.map(pokemon => {
      return _.pick(pokemon, pokemonFields);
    });
    const sortPokemons = _.sortBy(pokemons, pokemon => -pokemon.weight);

    return res.json(sortPokemons);
  } catch(err){
    console.log(err);
    return res.json({err})
  }
});

////////////////////// сложение двух чисел //////////////////////////////////

function summ(a = 0, b = 0) {
  return ((+a) + (+b));
}

app.get('/summ', (req,res) => {
  const sum = summ(req.query.a, req.query.b);

  res.send(`${sum}`);
});


////////////////// `Имя Отчество Фамилия` в `Фамилия И. О.` //////////////////


function fio(fullname) {
  if(fullname){
    fullname = fullname.split(' ');
    switch (fullname.length){
      case 3:
        return `${fullname[2]} ${fullname[0][0]}. ${fullname[1][0]}.`;
        break;
      case 2:
        return `${fullname[1]} ${fullname[0][0]}.`;
        break;
      case 1:
        return fullname[0];
        break;
    }
  }

  return 'Invalid fullname';

}

app.get('/', (req,res) => {
  console.log(req.query.fullname);
  const name = fio(req.query.fullname);

  res.send(name);
});



app.listen(3000, () => {
  console.log('Example app listening on port 3000')
});
