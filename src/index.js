import express from 'express';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

let pc = {};




async function getPc() {
  if (_.isEmpty(pc)){
    const response = await fetch('https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json');
    return response.json();
  } else {
    return pc;
  }

}

function getValue(pc, params, i = 0){
  let result;
  if (i >= params.length) return pc;
  result = pc[params[i]];
  if (_.isObject(result)) return getValue(result, params, ++i);
  return result;
}

function getVolumes(pc) {
  let volumes = {};
  pc["hdd"].forEach(function (volume) {
    if(!volumes[volume["volume"]]) volumes[volume["volume"]] = 0;
    volumes[volume["volume"]] += volume["size"];
  });
  console.log(volumes);
  for(let vol in volumes){
    volumes[vol] += "B";
  };
  return volumes;
}

app.get('/*', async (req,res) => {
  try {
    pc = await getPc();
    let params = req.params[0] ? req.params[0].split('/') : [];
    let result;
    if (params[0] === 'volumes'){
      result = getVolumes(pc);
    } else {
      console.log('params', params);
      result =  getValue(pc, params);
      console.log(result);
    }

    if (result !== undefined){
      res.statusCode = 200;
      return res.json(result);
    } else {
      res.statusCode = 404;
      return res.send('Not Found');
    }

  } catch(err){
    console.log(err);
    return res.json({err})
  }
});


app.listen(3000, () => {
  console.log('Example app listening on port 3000')
});
